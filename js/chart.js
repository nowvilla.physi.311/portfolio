$(function () {
    const skillCTX = $("#skill-graph");
    const skillLabels = [
        "HTML",
        "CSS",
        "Sass",
        "JavaScript",
        "jquery",
        "vue",
        "React",
        "PHP",
        "Laravel",
        "Python",
        "Django",
        "Flask",
        "Responder",
        "Java",
        "Android",
        "WordPress",
        "Git",
        "SEO",
        "Figma",
        "Illustrator",
        "Photoshop"
    ]
    const skillDegree = [
        5,
        5,
        5,
        4,
        4,
        2,
        0,
        3,
        1,
        5,
        3,
        4,
        4,
        3,
        4,
        0,
        4,
        1,
        2,
        0,
        0
    ]
    createGraph(
        skillCTX,
        skillLabels,
        skillDegree
    )
});

function createGraph(ctx, labels, degree) {
    ctx.attr("height", 400);
    const data = {
        labels: labels,
        datasets: [{
            label: "習熟度",
            data: degree,
            backgroundColor: "#e8c800",
        }]
    };

    const options = {
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true,
                    min: 0,
                    max: 5,
                    stepSize: 1,
                }
            }]
        }
    };

    new Chart(ctx, {
        type: "horizontalBar",
        data: data,
        options: options
    });
}
