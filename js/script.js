var sm = "400px";

$(document).ready(function () {
    $("#works").hide();
    $("#skills").hide();
    $("#contact").hide();
});

$(function () {
    deSVG(".turtle", true);
});

$(".tab").on("click", function () {
    const index = $(".tab").index(this);
    $(".tab").removeClass("active");
    $(this).addClass("active");
    $(".menu-container").hide().eq(index).fadeIn(300);
});

$(".view-more").on("click", function (e) {
    e.preventDefault();
    alert("sorry. coming soon...")
});
